package com.videogame.core.VideoGameVersion;

public  class Vocalist extends Performers {
	public Vocalist(int UnionId, String string) {
		super(UnionId, string);
		// TODO Auto-generated constructor stub
	}
	private String Key;
	public int UnionId;
	public int Volume;
	
	public String getKey() {
		return Key;
	}
	public void setKey() {
		this.Key=Key;
		
	}
	public int getUnionId() {
		return UnionId;
	}
	public void setUnionId() {
		this.UnionId=UnionId;
	}
	public int getVolume() {
		return Volume;
	}
	public void setVolume() {
		this.Volume=Volume;
	}
	@Override
	public String toString() {
		return "I sing in the key of " + Key+this.getUnionId()+" - Vocalist";
	}
	

	
}
